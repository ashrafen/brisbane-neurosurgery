<?php

/**
 * @file
 * Contains the url to banner argument default plugin.
 */

/**
 * Argument plugin to use the raw value from the URL and convert to banner.
 *
 * @ingroup views_argument_default_plugins
 */
class views_plugin_argument_default_banner_url extends views_plugin_argument_default {

  function get_argument() {
    $wildcard_paths = [];
    if ($node = menu_get_object('node')) {
      $wildcard_paths[] = 'node/' . $node->nid;
    }

    if ($alias = drupal_get_path_alias()) {
      // Split the path into wildcard matching paths.
      $path_parts = explode('/', $alias);
      $wildcard_paths[] = $alias;
      while (count($path_parts) > 1) {
        unset($path_parts[count($path_parts) - 1]);
        $wildcard_paths[] = implode('/', $path_parts) . '/*';
      }
    }


    // From most specific to least, we try and find a match.
    foreach ($wildcard_paths as $path) {
      // EFQ based on the path.
      $query = new EntityFieldQuery();
      $results = $query
        ->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'internal_banner')
        ->fieldCondition('field_display_on_these_paths', 'value', $path)
        ->execute();

      if (!empty($results['node'])) {
        $node = reset($results['node']);
        return $node->nid;
      }
    }

    return NULL;
  }

}
