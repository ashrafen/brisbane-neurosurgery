# Online Medical License

This module provides additional checks when installing an Online Medical module. At it's core this
module handles the firing of the Online Medical license modal window. The window is triggered when
the target module is defined in hook_om_license_module_info. The user installing the target module
has no other choice but to agree to the provided terms and conditions of use. This ensures our module
license has been given more than enough opportunity to be read and understood. 

Features:

* Provides ctools modal window integration.
* Provides one way reference; target module doesn't have to be aware of om_license.
* Works with standard module installation workflow.

Requirements:

* [ctools](https://www.drupal.org/project/ctools)

Installation:

* Install as you would a contributed drupal module, 
see https://drupal.org/documentation/install/modules-themes/modules-7

Configuration:

* None
