

(function ($, Drupal, window, document, undefined) {

	// Drupal.behaviors.omvbanner = {
	//   attach: function(context, settings) {


	//     $( document ).ready(function() {

	//     		var tc;
	//     		var cuepoints = [];
	// 		    var slideDuration = [];
	// 		    var transDuration = [];
	// 		    var cueID = 0;
	// 		    var vidDur;
	// 		    var slideDur;
	// 		    var transDur;
	// 		    var cuePoint;
	// 		    var vidCurTime;
	// 		    var omplayer;


			    
	// 		    // Video Banner 
	// 			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// 		    function omv_Init(){

	// 		    	omplayer = new MediaElement('omvid', {
	// 		    		alwaysShowControls: false,
	// 		    		loop: true,
					   
	// 				    success: function (omplayer, domObject) { 
					         
					       

	// 				        // Cuepoints
	// 				        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// 				        omplayer.addEventListener('timeupdate', function(e) {

	// 				            vidCurTime = Math.round(omplayer.currentTime * 10) / 10;
	// 							cuePoint = Number(cuepoints[cueID]);
	// 						    slideDur = Number(slideDuration[cueID]);
	// 						    transDur = Number(transDuration[cueID]);
								
	// 							// Display the video timecode
	// 							if($('.timecode').length > 0){
	// 					    		$('.timecode').html(Math.round(omplayer.currentTime));
	// 					    	}

	// 							// Fires on each cuepoint in the array
	// 						    if (vidCurTime >= cuePoint) {  

	// 						       	resetActive();

	// 						       	// Set the active class to the current cuepoint
	// 						        var c = ".cuepoint" + cueID;
	// 						        $(c).addClass("active");

	// 						        // Sets the slide to inactive after the slide duration expires
	// 						        var delay = slideDur*1000; 
	// 								setTimeout(function() {
	// 								  	$(c).removeClass("active");
	// 								}, delay);

	// 						        cueID++;	
	// 						    } 
	// 				        }, false);


	// 				         // Video end / Loop
	// 				         // ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// 				        omplayer.addEventListener('ended', function(e) {      
	// 				            resetActive();
	// 				            omplayer.play();
	// 			    			cueID = 0;

	// 				        }, false);


	// 				         omplayer.addEventListener('loadedmetadata', function(e) {      
	// 				            initVideoSize();
					            
	// 				        }, false);


	// 				      	// Start the video
	// 				     	omplayer.play();
	// 				    },
					 
	// 				    error: function () { 
					     
	// 				    }
	// 				});
	// 			}


	// 			function resetActive(){
	// 				$( ".cuepoint" ).each(function( index ) {
	// 				 	$(this).removeClass('active');
	// 				});
	// 			}






	// 			// Video Banner Resize
	// 			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// 			var vidHeight = $('.view-video-banner video').height();
	// 			var vidBanner = $('.view-video-banner');
	// 			vidBanner.height(vidHeight);


	// 	    	var delay = (function(){
	// 			var timer = 0;
	// 				return function(callback, ms){
	// 					clearTimeout (timer);
	// 					timer = setTimeout(callback, ms);
	// 				};
	// 			})();

	// 			// On window resize - recalculate the video size and throttle the callback
	// 			$(window).resize(function() {
	// 			    delay(function(){
	// 			     	vidHeight = $('.view-video-banner video').height();
	// 			     	vidBanner.height(vidHeight);
	// 			    }, 100);
	// 			});

	// 			// Get the size initially
	// 			function initVideoSize(){
	// 				 vidHeight = $('.view-video-banner video').height();
	// 				 vidBanner.height(vidHeight);
	// 			}



	// 			// Fallback banner and slideshow
	// 			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// 			var omvInterval;
	// 			var omvTimerID = 0;

	// 			function omvSlideshow() {

	// 				omvTimerID++;
	// 				resetActive();

	// 				//console.log(omvTimerID);

	// 				// Set the slide to active
	// 			    var c = ".cuepoint" + omvTimerID;
	// 				$(c).addClass("active");
					
	// 				if(omvTimerID >= cuepoints.length){
	// 					omvTimerID = 0;

	// 					c = ".cuepoint" + omvTimerID;
	// 					$(c).addClass("active");
	// 					console.log("reset");
	// 				} 

					
	// 			}


	// 			function omv_renderFallback(){

	// 				// Generate the fallback image
	// 				var poster_src = $('#omvid').data("fallback");
	// 				var url = "url(" + poster_src + ")";
	// 				$('.view-video-banner').css("background-image", url);  

	// 				// Start the slideshow
	// 				omvSlideshow();

	// 				// Run the slideshow every x secs
	// 				omvInterval = setInterval( function(){ 
	// 					omvSlideshow();
	// 				}, 6000);
	// 			}

				 
				

	// 			// Window load
	// 			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// 			window.onload = function () {
					
	// 				// Collect all cuepoint data
	// 				$( ".cuepoint" ).each(function( index ) {
	// 				 	cuepoints.push($( this ).data("cuepoint"));
	// 				 	slideDuration.push($( this ).data("duration"));
	// 				 	transDuration.push($( this ).data("transition"));
	// 				});



	// 				// On mobile
	// 				// Render the fallback image and slideshow

	// 				if($('body').hasClass('mobile')){
	// 					omv_renderFallback();
	// 				} else{

	// 					// On Dektops
	// 					// If the video banner exsists – fire it up
	// 					if($('.view-video-banner').length > 0){
	// 				    	omv_Init();
	// 				    }
	// 				}				
	// 			};






	// 	   });
	//     }
	// };


})(jQuery, Drupal, this, this.document);



