/* Implement custom javascript here */

(function ($, Drupal, window, document, undefined) {

	Drupal.behaviors.my_custom_behavior = {
	  	attach: function(context, settings) {
			// Convert flexslider slideshow inline images into background images
			$('.front .flexslider .slides > li > img').replaceWith(function(){
			return $('<div>', {
				style: 'background-image: url('+this.src+')',
				class: 'slideshow__image'
			});
		});

		var bannerURL = $('.node .field-name-field-banner-image > img').attr("src");
		$('.banner_image_dynamic').css('background-image', 'url(' + bannerURL + ')');


        $(".features-item").click(function() {
        	$(this).addClass('clicked');
          window.location = $(this).find("a").first().attr("href");
        });

	    $( document ).ready(function() {
	    	/*fix paremt menu active class*/
	    	$('#main-menu a.active').parents('.has-dropdown').find('>a').addClass('active');









		   });
	    }
	};


})(jQuery, Drupal, this, this.document);



