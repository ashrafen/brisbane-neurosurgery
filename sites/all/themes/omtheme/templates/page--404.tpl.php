<!--.page -->
<div role="document" class="page">
    <!--.l-header -->
    <header class="l-header">

        <?php if ($top_bar): ?>
            <!--.top-bar -->
            <?php if ($top_bar_classes): ?>
                <div class="<?php print $top_bar_classes; ?>">
            <?php endif; ?>
            <div class="row">
                <div class="logo small-6 small-offset-0 large-4 large-offset-0 columns "><a href="/"><img src="<?php print base_path().'sites/all/themes/omtheme/logo.png';?>" alt="Brisbane Neurosurgery Advanced Care" class="logo__image"/></a></div>
                <div class="header-phonenumber-wrap small-2 small-offset-1 large-3 columns">
                    <a class="header-phonenumber" href="tel:+61738328866"><span>CALL NOW</span>07 3832 8866</a>
                </div>
            </div>

            <div class="nav-wrap">
                <nav class="top-bar" data-topbar <?php print $top_bar_options; ?>>
                    <ul class="title-area header-content row">
                        <li class="toggle-topbar menu-icon small-2 columns"><a href="#"></a></li>
                    </ul>
                    <section class="top-bar-section">
                        <?php if ($top_bar_main_menu) :?>
                            <?php print $top_bar_main_menu; ?>
                        <?php endif; ?>
                        <?php if ($top_bar_secondary_menu) :?>
                            <?php print $top_bar_secondary_menu; ?>
                        <?php endif; ?>
                    </section>
                </nav>
            </div>
            <?php if ($top_bar_classes): ?>
                </div>
            <?php endif; ?>
            <!--/.top-bar -->
        <?php endif; ?>

        <!-- Title, slogan and menu -->
        <?php if ($alt_header): ?>
            <section class="row <?php print $alt_header_classes; ?>">

                <?php if ($linked_logo): print $linked_logo; endif; ?>

                <?php if ($site_name): ?>
                    <?php if ($title): ?>
                        <div id="site-name" class="element-invisible">
                            <strong>
                                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                            </strong>
                        </div>
                    <?php else: /* Use h1 when the content title is empty */ ?>
                        <h1 id="site-name">
                            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                        </h1>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if ($site_slogan): ?>
                    <h2 title="<?php print $site_slogan; ?>" class="site-slogan"><?php print $site_slogan; ?></h2>
                <?php endif; ?>

                <?php if ($alt_main_menu): ?>
                    <nav id="main-menu" class="navigation" role="navigation">
                        <?php print ($alt_main_menu); ?>
                    </nav> <!-- /#main-menu -->
                <?php endif; ?>

                <?php if ($alt_secondary_menu): ?>
                    <nav id="secondary-menu" class="navigation" role="navigation">
                        <?php print $alt_secondary_menu; ?>
                    </nav> <!-- /#secondary-menu -->
                <?php endif; ?>

            </section>
        <?php endif; ?>
        <!-- End title, slogan and menu -->

        <?php if (!empty($page['header'])): ?>
            <!--.l-header-region -->
            <section class="l-header-region row">
                <div class="">
                    <?php print render($page['header']); ?>
                </div>
            </section>
            <!--/.l-header-region -->
        <?php endif; ?>

    </header>

    <?php if ($messages && !$zurb_foundation_messages_modal): ?>
        <!--.l-messages -->
        <section class="l-messages row">
            <div class="columns">
                <?php if ($messages): print $messages; endif; ?>
            </div>
        </section>
        <!--/.l-messages -->
    <?php endif; ?>

    <?php if (!empty($page['help'])): ?>
        <!--.l-help -->
        <section class="l-help row">
            <div class="columns">
                <?php print render($page['help']); ?>
            </div>
        </section>
        <!--/.l-help -->
    <?php endif; ?>

    <!--.l-main -->
    <main role="main" class="row l-main">
        <!-- .l-main region -->
        <div class="<?php print $main_grid; ?> main columns">
            <?php if (!empty($page['highlighted'])): ?>
                <div class="highlight panel callout">
                    <?php print render($page['highlighted']); ?>
                </div>
            <?php endif; ?>

            <a id="main-content"></a>

            <?php if ($breadcrumb): print $breadcrumb; endif; ?>

            <?php if ($title): ?>
                <?php print render($title_prefix); ?>
                <h1 id="page-title" class="title"><?php print $title; ?></h1>
                <?php print render($title_suffix); ?>
            <?php endif; ?>

            <?php if (!empty($tabs)): ?>
                <?php print render($tabs); ?>
                <?php if (!empty($tabs2)): print render($tabs2); endif; ?>
            <?php endif; ?>

            <?php if ($action_links): ?>
                <ul class="action-links">
                    <?php print render($action_links); ?>
                </ul>
            <?php endif; ?>

            <?php print render($page['content']); ?>
        </div>
        <!--/.l-main region -->
    </main>
    <!--/.l-main -->

    <?php if (!empty($page['footer_firstcolumn']) || !empty($page['footer_secondcolumn']) || !empty($page['footer_thirdcolumn']) || !empty($page['footer_fourthcolumn'])): ?>
        <!--.footer-columns -->
        <section class="row l-footer-columns">
            <?php if (!empty($page['footer_firstcolumn'])): ?>
                <div class="footer-first medium-3 columns">
                    <?php print render($page['footer_firstcolumn']); ?>
                </div>
            <?php endif; ?>
            <?php if (!empty($page['footer_secondcolumn'])): ?>
                <div class="footer-second medium-3 columns">
                    <?php print render($page['footer_secondcolumn']); ?>
                </div>
            <?php endif; ?>
            <?php if (!empty($page['footer_thirdcolumn'])): ?>
                <div class="footer-third medium-3 columns">
                    <?php print render($page['footer_thirdcolumn']); ?>
                </div>
            <?php endif; ?>
            <?php if (!empty($page['footer_fourthcolumn'])): ?>
                <div class="footer-fourth medium-3 columns">
                    <?php print render($page['footer_fourthcolumn']); ?>
                </div>
            <?php endif; ?>
        </section>
        <!--/.footer-columns-->
    <?php endif; ?>

    <!--.l-footer -->
    <footer>
        <div  class="l-footer row" role="contentinfo">
            <?php if (!empty($page['footer'])): ?>
                <div class="feature small-6 large-6 columns move-to-right">
                    <?php print render($page['footer']); ?>
                </div>
            <?php endif; ?>

            <?php if ($site_name) : ?>
                <div class="feature small-6 large-6 columns move-to-left">
                    <a href="/" class="footer-logo">
                        <img src="/sites/all/themes/omtheme/images/footer-logo.png" alt="Brisbane Neurosurgery Advanced Care">
                    </a>
                    <p class="copyright">&copy; Dr Scott Campbell, 2018 <br/> Website by <a href="https://www.onlinemedical.com.au/" title="">Online Medical</a></p>
                </div>
            <?php endif; ?>
        </div>

    </footer>
    <!--/.l-footer -->
    <?php if ($messages && $zurb_foundation_messages_modal): print $messages; endif; ?>
</div>
<!--/.page -->
