<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php $images = [
  1 => "/sites/all/themes/omtheme/images/doctor-icon.png",
  2 => "/sites/all/themes/omtheme/images/clinical-icon.png",
  3 => "/sites/all/themes/omtheme/images/legal-icon.png",
  4 => "/sites/all/themes/omtheme/images/doctor-icon-white.png",
  5 => "/sites/all/themes/omtheme/images/clinical-icon-white.png",
  6 => "/sites/all/themes/omtheme/images/legal-icon-white.png"
];
  $i = 1;?>
<section class="features">
  <div class="l-triptych row">
<?php foreach ($rows as $id => $row): ?>
  <div class="feature small-4 large-4 columns">
  <div class="features-item">
      <img src="<?php print $images[$i];?>" alt="">
      <img class=" white-img" src="<?php print $images[$i+3];?>" alt="">
      <?php print $row;$i++; ?>
  </div>
  </div>
<?php endforeach; ?>
  </div>
</section>

