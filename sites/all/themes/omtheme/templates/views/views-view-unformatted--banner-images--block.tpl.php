<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
  <section class="block block-views bannerimage__wrapper block-views-banner-images-block">
<?php foreach ($rows as $id => $row): ?>
    <picture>
        <?php print $row; ?>
    </picture>
<?php endforeach; ?>
  </section>
