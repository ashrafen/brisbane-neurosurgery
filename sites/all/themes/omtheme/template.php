<?php

/**
 * Implements template_preprocess_html().
 */
function omtheme_preprocess_html(&$variables) {
}

/**
 * Implements template_preprocess_page.
 */
function omtheme_preprocess_page(&$variables) {
    $status = drupal_get_http_header("status");
    if ($status === '404 Not Found' || $status === '403 Forbidden') {
        $variables['theme_hook_suggestions'][] = 'page__404';
    }
}

/**
 * Implements template_preprocess_node.
 */
function omtheme_preprocess_node(&$variables) {
}
